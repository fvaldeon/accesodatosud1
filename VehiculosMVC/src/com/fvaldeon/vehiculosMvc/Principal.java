package com.fvaldeon.vehiculosMvc;

import com.fvaldeon.vehiculosMvc.gui.VehiculosControlador;
import com.fvaldeon.vehiculosMvc.gui.VehiculosModelo;
import com.fvaldeon.vehiculosMvc.gui.Ventana;

import java.io.IOException;

/**
 * Created by Profesor on 10/10/2017.
 */
public class Principal {
    public static void main(String[] args) {
        Ventana vista = new Ventana();
        VehiculosModelo modelo = new VehiculosModelo();
        VehiculosControlador controlador = new VehiculosControlador(vista,modelo);

    }

}
