package com.fvaldeon.vehiculosMvc.gui;

import com.fvaldeon.vehiculosMvc.base.Vehiculo;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * Created by Profesor on 05/10/2017.
 */
public class Ventana {
    private JPanel panel1;
    public JRadioButton cocheRadioButton;
    public JRadioButton motoRadioButton;
    public JTextField modeloTxt;
    public JTextField marcaTxt;
    public JTextField matriculaTxt;
    public JTextField kmsPlazasTxt;
    public JList<Vehiculo> list1;
    public JButton importarBtn;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JLabel plazasKmsLbl;
    public DatePicker fechaMatriculaDPicker;
    public JFrame frame;

    //Elementos declarados a mano
    public DefaultListModel<Vehiculo> dlmVehiculo;

    public Ventana() {
        frame = new JFrame("VehiculosMVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents(){
        dlmVehiculo = new DefaultListModel<Vehiculo>();
        list1.setModel(dlmVehiculo);
    }

}
