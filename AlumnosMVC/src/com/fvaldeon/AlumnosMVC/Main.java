package com.fvaldeon.AlumnosMVC;

import com.fvaldeon.AlumnosMVC.com.fvaldeon.AlumnosMVC.gui.AlumnosControlador;
import com.fvaldeon.AlumnosMVC.com.fvaldeon.AlumnosMVC.gui.AlumnosModelo;
import com.fvaldeon.AlumnosMVC.com.fvaldeon.AlumnosMVC.gui.Ventana1;

/**
 * Created by Profesor on 26/09/2017.
 */
public class Main {

    public static void main(String[] args) {
        Ventana1 vista = new Ventana1();
        AlumnosModelo modelo = new AlumnosModelo();
        AlumnosControlador controlador = new AlumnosControlador(vista, modelo);
    }

}
