package com.fvaldeon.AlumnosMVC.com.fvaldeon.AlumnosMVC.gui;

import com.fvaldeon.AlumnosMVC.com.fvaldeon.AlumnosMVC.base.Alumno;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by Fernando on 26/09/2017.
 */
public class AlumnosControlador implements ActionListener, KeyListener, ListSelectionListener {
    private AlumnosModelo modelo;
    private Ventana1 vista;


    public AlumnosControlador(Ventana1 vista, AlumnosModelo modelo){
        this.modelo = modelo;
        this.vista = vista;

        anadirActionListeners(this);
        anadirKeyListener(this);
        anadirListSelectionListener(this);
    }

    private void anadirActionListeners(ActionListener listener){
        vista.nuevoBtn.addActionListener(listener);
        vista.borrarBtn.addActionListener(listener);
    }

    private void anadirKeyListener(KeyListener listener){

        vista.buscarTxt.addKeyListener(listener);
    }
    private void anadirListSelectionListener(ListSelectionListener listener){
        vista.list.addListSelectionListener(listener);
    }

    /*
    Se ejecuta cuando se recibe un clic sobre el boton "eliminar" o "nuevo"
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch(actionCommand){
            case "Nuevo":
                if(faltanDatos()){
                    JOptionPane.showMessageDialog(null, "Los campos Nombre, Apellidos, Dni y Fecha de Nacimiento son obligatorios","Error", JOptionPane.ERROR_MESSAGE);
                    break;
                }
                if(modelo.existeDni(vista.dniTxt.getText())){
                    JOptionPane.showMessageDialog(null, "El dni: "+vista.dniTxt.getText()+" ya existe","Error", JOptionPane.ERROR_MESSAGE);
                    break;
                }
                modelo.altaAlumno(vista.nombreTxt.getText(), vista.apellidosTxt.getText(), vista.dniTxt.getText(), vista.fechaNacimientoDatePicker.getDate());
                borrarCampos();
                refrescar();
                break;

            case "Borrar":
                if(faltaDni()){
                    JOptionPane.showMessageDialog(null, "Debes indicar un dni para borrar","Error", JOptionPane.ERROR_MESSAGE);
                    break;
                }
                if(!modelo.existeDni(vista.dniTxt.getText())) {
                    JOptionPane.showMessageDialog(null, "El dni: " + vista.dniTxt.getText() + " no existe", "Error", JOptionPane.ERROR_MESSAGE);
                    break;
                }
                modelo.eliminarAlumno(vista.dniTxt.getText());
                borrarCampos();
                refrescar();
                break;
        }
    }

    /*
    Deja todos los campos en blanco
     */
    private void borrarCampos() {
        vista.dniTxt.setText(null);
        vista.fechaNacimientoDatePicker.setText(null);
        vista.apellidosTxt.setText(null);
        vista.nombreTxt.setText(null);
        vista.buscarTxt.setText(null);
    }

    /*
    Me indica si algún campo de texto esta vacio
     */
    private boolean faltanDatos() {
        if(vista.dniTxt.getText().isEmpty() || vista.apellidosTxt.getText().isEmpty()
                || vista.nombreTxt.getText().isEmpty() || vista.fechaNacimientoDatePicker.getText().isEmpty()){
            return true;
        }
        return false;
    }

    /*
    Me indica si el campo de texto dni esta vacío
     */
    private boolean faltaDni(){
        if(vista.dniTxt.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    /*
    Refresca el Jlist con los alumnos del ArrayList
     */
    private void refrescar(){
        vista.dlmAlumno.clear();
        for(Alumno unAlumno : modelo.obtenerAlumnos()){
            vista.dlmAlumno.addElement(unAlumno);
        }
    }

    /*
    Refresca el Jlist con los alumnos cuyo dni empieza por "cadena"
    */
    private void refrescar(String cadena){
        vista.dlmAlumno.clear();
        for(Alumno unAlumno : modelo.obtenerAlumnos()){
            if(unAlumno.getDni().startsWith(cadena)) {
                vista.dlmAlumno.addElement(unAlumno);
            }
        }
    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /*
    Cada vez que escribe un caracter en el campo busqueda se actualiza el Jlist
     */
    @Override
    public void keyReleased(KeyEvent e) {

        refrescar(vista.buscarTxt.getText());
    }

    /*
    Cada vez que se selecciona un alumno de la JList se muestran sus datos en los campos de texto
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()) {
            Alumno unAlumno = (Alumno) vista.list.getSelectedValue();
            vista.dniTxt.setText(unAlumno.getDni());
            vista.nombreTxt.setText(unAlumno.getNombre());
            vista.fechaNacimientoDatePicker.setDate(unAlumno.getFechaNacimiento());
            vista.apellidosTxt.setText(unAlumno.getApellidos());
        }
    }
}
