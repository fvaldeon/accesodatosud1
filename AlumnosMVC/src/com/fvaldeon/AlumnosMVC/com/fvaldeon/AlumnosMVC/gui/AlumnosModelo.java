package com.fvaldeon.AlumnosMVC.com.fvaldeon.AlumnosMVC.gui;

import com.fvaldeon.AlumnosMVC.com.fvaldeon.AlumnosMVC.base.Alumno;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by Fernando on 26/09/2017.
 */
public class AlumnosModelo {
    private ArrayList<Alumno> listaAlumnos;

    public AlumnosModelo(){
        listaAlumnos = new ArrayList<Alumno>();
    }

    public void altaAlumno(String nombre, String apellidos, String dni, LocalDate fechaNacimiento){
        Alumno nuevoAlumno = new Alumno(nombre, apellidos, dni, fechaNacimiento);
        listaAlumnos.add(nuevoAlumno);
    }


    public void eliminarAlumno(String dni){
        listaAlumnos.remove(buscarAlumno(dni));
    }


    public Alumno buscarAlumno(String dni){
        for (Alumno unAlumno: listaAlumnos) {
            if(unAlumno.getDni().equals(dni)){
                return unAlumno;
            }
        }
        return null;
    }

    public ArrayList<Alumno> obtenerAlumnos(){
        return listaAlumnos;
    }

    public boolean existeDni(String dni){
        if(buscarAlumno(dni) == null){
            return false;
        }
            return true;
    }

}
