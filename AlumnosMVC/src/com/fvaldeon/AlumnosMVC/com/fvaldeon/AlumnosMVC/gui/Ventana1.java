package com.fvaldeon.AlumnosMVC.com.fvaldeon.AlumnosMVC.gui;

import com.fvaldeon.AlumnosMVC.com.fvaldeon.AlumnosMVC.base.Alumno;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * Created by Profesor on 26/09/2017.
 */
public class Ventana1 {
    private JPanel panel1;
    public JTextField nombreTxt;
    public JTextField apellidosTxt;
    public JTextField dniTxt;
    public JTextArea alumnoTxtArea;
    public JButton listarBtn;
    public JButton nuevoBtn;
    public JButton borrarBtn;
    private JLabel fechaNacimientoLb;
    public DatePicker fechaNacimientoDatePicker;
    public JList list;
    public JTextField buscarTxt;

    public DefaultListModel<Alumno> dlmAlumno;


    public Ventana1() {
        JFrame frame = new JFrame("GestorAlumnosMVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        iniciarJList();
    }

    private void iniciarJList(){
        dlmAlumno = new DefaultListModel<Alumno>();
        list.setModel(dlmAlumno);
    }
}
