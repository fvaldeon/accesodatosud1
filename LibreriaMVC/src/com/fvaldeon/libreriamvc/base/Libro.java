package com.fvaldeon.libreriamvc.base;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by DAM on 18/09/2018.
 */
public class Libro implements Serializable{
    private String titulo;
    private String autor;
    private String isbn;
    private LocalDate fecha;

    public Libro(String titulo1, String autor1, String isbn1, LocalDate fecha) {
        this.titulo = titulo1;
        this.autor = autor1;
        this.isbn = isbn1;
        this.fecha = fecha;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }


    @Override
    public String toString() {
        return titulo + " - " + autor;

    }
}
