package com.fvaldeon.libreriamvc.base;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by DAM on 18/09/2018.
 */
public class ModeloLibreria {

    private ArrayList<Libro> libros;

    public ModeloLibreria() {

        libros = new ArrayList<>();
    }


    public void altaLibro(String titulo, String autor, String isbn, LocalDate fecha){
        libros.add(new Libro(titulo, autor, isbn, fecha));
    }

    public ArrayList<Libro> obtenerLibros(){
        return libros;
    }

    public void eliminarLibro(Libro libro){
        libros.remove(libro);
    }

    public void cargarLibrosDeFichero(File fichero){

        if(!fichero.exists()){
            return;
        }

        FileInputStream fis = null;
        ObjectInputStream ois;

        try {
            fis = new FileInputStream(fichero);
            ois = new ObjectInputStream(fis);

            libros = (ArrayList<Libro>)ois.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {

            if(fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public void guardarLibrosEnFichero(File fichero)  {
        FileOutputStream fos = null;
        ObjectOutputStream oos;

        try {
            fos = new FileOutputStream(fichero);
            oos = new ObjectOutputStream(fos);

            oos.writeObject(libros);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            if(fos != null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public void exportarXML(File selectedFile) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();

        Document documento = dom.createDocument(null, "xml", null);

        //Creo el primer nodo del fichero XML
        //Corresponde a la etiqueta "libreria"
        Element raiz = documento.createElement("libreria");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoLibro = null;
        Element nodoDatos = null;
        Text dato = null;

        for(Libro libro : libros){
            //Creo un nodo libro por cada libro de la lista
            nodoLibro = documento.createElement("libro");
            raiz.appendChild(nodoLibro);

            //En cada libro creo un nodo para cada campo
            nodoDatos = documento.createElement("titulo");
            nodoLibro.appendChild(nodoDatos);
            //En cada campo anado el valor de su texto
            dato = documento.createTextNode(libro.getTitulo());
            nodoDatos.appendChild(dato);

            nodoDatos = documento.createElement("autor");
            nodoLibro.appendChild(nodoDatos);
            dato = documento.createTextNode(libro.getAutor());
            nodoDatos.appendChild(dato);

            nodoDatos = documento.createElement("isbn");
            nodoLibro.appendChild(nodoDatos);
            dato = documento.createTextNode(libro.getIsbn());
            nodoDatos.appendChild(dato);

            nodoDatos = documento.createElement("fecha-publicacion");
            nodoLibro.appendChild(nodoDatos);
            dato = documento.createTextNode(libro.getFecha().toString());
            nodoDatos.appendChild(dato);
        }

        Source src = new DOMSource(documento);
        Result resultado = new StreamResult(selectedFile);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(src, resultado);

    }

    public void importarXML(File selectedFile) throws ParserConfigurationException, IOException, SAXException {
        //Elimino los libros anteriores


        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(selectedFile);

        NodeList nodoslibros = document.getElementsByTagName("libro");

        for ( int i = 0; i < nodoslibros.getLength(); i++){


            Element libro = (Element) nodoslibros.item(i);

            String titulo = libro.getElementsByTagName("titulo").item(0).getChildNodes().item(0).getNodeValue();
            String autor = libro.getElementsByTagName("autor").item(0).getChildNodes().item(0).getNodeValue();
            String isbn = libro.getElementsByTagName("isbn").item(0).getChildNodes().item(0).getNodeValue();;
            String fecha = libro.getElementsByTagName("fecha-publicacion").item(0).getChildNodes().item(0).getNodeValue();;

            altaLibro(titulo, autor, isbn, LocalDate.parse(fecha));
        }

    }
}
