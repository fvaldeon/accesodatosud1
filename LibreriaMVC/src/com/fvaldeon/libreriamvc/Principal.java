package com.fvaldeon.libreriamvc;

import com.fvaldeon.libreriamvc.base.Libro;
import com.fvaldeon.libreriamvc.base.ModeloLibreria;
import com.fvaldeon.libreriamvc.gui.Controlador;
import com.fvaldeon.libreriamvc.gui.Vista;

import java.io.File;



/**
 * Created by fvaldeon on 26/09/2018.
 */
public class Principal {

    public static void main(String[] args) {
        Vista vista = new Vista();
        ModeloLibreria modelo = new ModeloLibreria();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
