package com.fvaldeon.libreriamvc.util;

import javax.swing.*;

/**
 * Created by DAM on 28/09/2018.
 */
public class Util {
    /**
     * Muestra una dialogo de error con el mensaje
     * @param mensaje a mostrar
     */
    public static void mensajeError(String mensaje){
        JOptionPane.showMessageDialog(null, mensaje,"Error", JOptionPane.ERROR_MESSAGE);
    }
}
