package com.fvaldeon.libreriamvc.gui;

import com.fvaldeon.libreriamvc.base.Libro;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;


/**
 * Created by DAM on 25/09/2018.
 */
public class Vista {
    private JPanel panel1;
    JTextField txtTitulo;
    JTextField txtAutor;
    JTextField txtIsbn;
    JList<Libro> lista;
    DatePicker datePicker;
    JButton modificarBtn;
    JButton altaBtn;
    JButton eliminarBtn;
    //Creo un modelo para trabajar con el JList
    DefaultListModel<Libro> dlmLibros;

    //El JFrame es atributo de la clase para poder acceder a él
    JFrame frame;
    JMenuItem itemGuardar;
    JMenuItem itemCargar;
    JMenuItem itemExportarXML;
    JMenuItem itemImportarXML;

    public Vista() {
        frame = new JFrame("Libreria MVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.pack();

        //Emplazo la ventana en el centro de la pantalla
        frame.setLocationRelativeTo(null);

        //Anado el modelo a el jlist
        iniciarLista();

        //Anado la barra de menus
        crearMenu();


        frame.setVisible(true);
    }

    private void iniciarLista(){
        dlmLibros = new DefaultListModel<>();
        lista.setModel(dlmLibros);
    }

    /**
     * Método que inicia una barra de menus
     */
    private void crearMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemGuardar = new JMenuItem("Guardar");
        itemCargar = new JMenuItem("Cargar");
        //Anado los actions commands a estos dos botones
        itemCargar.setActionCommand("Cargar");
        itemGuardar.setActionCommand("Guardar");
        itemExportarXML = new JMenuItem("Exportar XML");
        itemImportarXML = new JMenuItem(("Importar XML"));
        itemExportarXML.setActionCommand("ExportarXML");
        itemImportarXML.setActionCommand("ImportarXML");


        menu.add(itemCargar);
        menu.add(itemGuardar);
        menu.add(itemExportarXML);
        menu.add(itemImportarXML);
        barra.add(menu);

        frame.setJMenuBar(barra);
    }
}
