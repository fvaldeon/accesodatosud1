package com.fvaldeon.libreriamvc.gui;

import com.fvaldeon.libreriamvc.base.Libro;
import com.fvaldeon.libreriamvc.base.ModeloLibreria;
import com.fvaldeon.libreriamvc.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;

/**
 * Created by DAM on 25/09/2018.
 */
public class Controlador implements ActionListener, ListSelectionListener, WindowListener{

    private Vista vista;
    private ModeloLibreria modelo;

    public Controlador(Vista vista, ModeloLibreria modelo) {
        this.vista = vista;
        this.modelo = modelo;

        anadirActionListeners(this);
        anadirSelectionListeners(this);
        anadirWindowListeners(this);

        //Cargo desde fichero
        modelo.cargarLibrosDeFichero(new File("libros.bin"));

        listarLibros();
    }

    /**
     * Añado listeners a los botones de la vista
     */
    private void anadirActionListeners(ActionListener listener){
        vista.eliminarBtn.addActionListener(listener);
        vista.modificarBtn.addActionListener(listener);
        vista.altaBtn.addActionListener(listener);
        vista.itemGuardar.addActionListener(listener);
        vista.itemCargar.addActionListener(listener);
        vista.itemImportarXML.addActionListener(listener);
        vista.itemExportarXML.addActionListener(listener);
    }

    /**
     * Anado listener de seleccion en el Jlist
     * @param listener el objeto listener
     */
    private void anadirSelectionListeners(ListSelectionListener listener) {
        vista.lista.addListSelectionListener(listener);
    }

    /**
     * Anado el listener a la ventana de mi aplicacion
     * @param listener el objeto que actua como escuchador de eventos
     */
    private void anadirWindowListeners(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){

            case "altaLibro":
                //Codigo referente al evento del boton alta
                if(!faltanDatos()) {
                    modelo.altaLibro(vista.txtTitulo.getText(), vista.txtAutor.getText(),
                            vista.txtIsbn.getText(), vista.datePicker.getDate());
                    borrarCamposTexto();
                }else{
                    Util.mensajeError("Faltan datos");
                }

                break;

            case "eliminar":
                //codigo referente al evento sobre el boton eliminar
                Libro eliminado = vista.lista.getSelectedValue();
                modelo.eliminarLibro(eliminado);

                borrarCamposTexto();
                break;

            case "modificarLibro":
                    //boton modificar
                    Libro modificado = vista.lista.getSelectedValue();

                    if(modificado != null && !faltanDatos()) {
                        cambiarPropiedadesLibro(modificado);
                    }else{
                        Util.mensajeError("Debes seleccionar un libro y dar valor a todos los datos");
                    }

                break;
            case "Cargar": {
                JFileChooser selector = new JFileChooser();
                int opt = selector.showOpenDialog(null);

                if (opt == JFileChooser.APPROVE_OPTION) {
                    File fichero = selector.getSelectedFile();
                    modelo.cargarLibrosDeFichero(fichero);
                }
            }

                break;
            case "Guardar": {
                JFileChooser selector = new JFileChooser();
                int opt = selector.showSaveDialog(null);

                if (opt == JFileChooser.APPROVE_OPTION) {
                    File fichero = selector.getSelectedFile();
                    modelo.guardarLibrosEnFichero(fichero);
                }
            }
                break;
            case "ExportarXML": {
                JFileChooser selector = new JFileChooser();

                selector.setFileFilter(new FileNameExtensionFilter(
                        "Ficheros XML", "xml"));
                int opt = selector.showSaveDialog(null);

                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {

                        modelo.exportarXML(selector.getSelectedFile());

                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (TransformerException e1) {
                        e1.printStackTrace();
                    }
                }
            }
            break;

            case "ImportarXML":  {
                JFileChooser selector = new JFileChooser();

                selector.setFileFilter(new FileNameExtensionFilter("Ficheros XML", "xml"));

                int opt = selector.showOpenDialog(null);

                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selector.getSelectedFile());
                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (SAXException e1) {
                        e1.printStackTrace();
                    }
                }

            }
            break;

        }

        listarLibros();
    }

    /**
     * Método que me indica si hay valores en los campos de texto
     *
     * @return true si hay algún campo vacío, false en caso contrario
     *
     */

    private boolean faltanDatos(){
        return vista.txtIsbn.getText().isEmpty() ||
                vista.txtAutor.getText().isEmpty() ||
                vista.txtTitulo.getText().isEmpty() ||
                vista.datePicker.getText().isEmpty();
    }

    /**
     *  Borra el contenido de los campos de texto
     */
    private void borrarCamposTexto(){
        vista.txtIsbn.setText("");
        vista.txtTitulo.setText("");
        vista.txtAutor.setText("");
        vista.datePicker.setText("");
    }

    /**
     * Método que recibe un libro y le asigna los valores de los campos de texto
     * @param modificado objeto libro al que se le asignan los valores
     */

    private void cambiarPropiedadesLibro(Libro modificado) {
        modificado.setAutor(vista.txtAutor.getText());
        modificado.setTitulo(vista.txtTitulo.getText());
        modificado.setIsbn(vista.txtIsbn.getText());
        modificado.setFecha(vista.datePicker.getDate());
    }

    /**
     * Obtengo los atributos de un libro y los muestro en los
     * campos de texto
     * @param libro libro cuyos datos mostrare
     */
    private void obtenerPropiedadesYMostrar(Libro libro){
        vista.txtAutor.setText(libro.getAutor());
        vista.txtTitulo.setText(libro.getTitulo());
        vista.txtIsbn.setText(libro.getIsbn());
        vista.datePicker.setDate(libro.getFecha());
    }

    /**
     * Metodo encargado de listar los libros de la libreria
     */

    private void listarLibros(){
        //Vacio el Jlist
        vista.dlmLibros.clear();

        //Recorro la lista de libros de la libreria
        for(Libro libro : modelo.obtenerLibros()){

            //anado cada libro al defaultlistmodel (JList)
            vista.dlmLibros.addElement(libro);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        //Obtengo el libro seleccionado
        Libro seleccionado = vista.lista.getSelectedValue();

        //Si hay un libro seleccionado
        if(seleccionado  != null){
            //muestro sus datos en los campos de texto
            obtenerPropiedadesYMostrar(seleccionado);
        }else{
            borrarCamposTexto();
        }


    }


    @Override
    public void windowClosing(WindowEvent e) {
        modelo.guardarLibrosEnFichero(new File("libros.bin"));
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
