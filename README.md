** Ejercicios realizados en clase del RA1 de Acceso a Datos **

   * **AlumnosMVC** : Aplicaci�n para dar de alta alumnos, buscarlos y borrarlos. Gesti�n de eventos ActionEvent, KeyEvent y ListSelectionEvent. Patr�n Modelo-Vista-Controlador
   * **VehiculosMVC** : Aplicaci�n Modelo-Vista-Controlador, para gestionar Vehiculos de tipo Coche o Moto. Exportar e Importar XML, JFileChooser, Eventos de Ventana
   * **JListSuprimir** : Ventana con un bot�n que me a�ade cadenas de texto a un Jlist y a un JComboBox. Permite borrarlas con la tecla suprimir.
   * **LoginYFichConf** : Aplicaci�n que usa un cuadro de dialogo para introducir unos datos, y los guarda y carga en un fichero de configuracion.
   * **LibreriaMVC**: Aplicaci�n para gestionar libros en fichero: JList, datepicker, eventos de boton, Exportar e Importar XML
   * **EditorMVC**: Sencillo editor de texto, con un JTextArea, que permite guardar el texto en fichero y cargarlo
   * **EquiposMVC**: Programa que permite dar de alta equipos y entrenadores y asignar un entrenador a los equipos. ComboBox, JList, JDialog, Serializar

