import com.fvaldeon.equiposmvc.gui.Controlador;
import com.fvaldeon.equiposmvc.gui.Modelo;
import com.fvaldeon.equiposmvc.gui.Vista;

import java.io.IOException;

/**
 * Created by DAM on 23/10/2018.
 */
public class Principal {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
