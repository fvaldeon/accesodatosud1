package com.fvaldeon.equiposmvc.base;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by DAM on 25/10/2018.
 */
public class Entrenador implements Serializable{
    private String nombre;
    private String dni;
    private LocalDate fechaNacimiento;

    public Entrenador(String nombre, String dni, LocalDate fechaNacimiento) {
        this.nombre = nombre;
        this.dni = dni;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return nombre + " - " + dni;
    }
}
