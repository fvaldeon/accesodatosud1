package com.fvaldeon.equiposmvc.base;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by DAM on 25/10/2018.
 */
public class Equipo implements Serializable{
    private String nombre;
    private String sede;
    private LocalDate fechaFundacion;
    private Entrenador entrenador;

    public Equipo(String nombre, String sede, LocalDate fechaFundacion, Entrenador entrenador) {
        this.nombre = nombre;
        this.sede = sede;
        this.fechaFundacion = fechaFundacion;
        this.entrenador = entrenador;
    }

    public Equipo(String nombre, String sede, LocalDate fechaFundacion) {
        this.nombre = nombre;
        this.sede = sede;
        this.fechaFundacion = fechaFundacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public LocalDate getFechaFundacion() {
        return fechaFundacion;
    }

    public void setFechaFundacion(LocalDate fechaFundacion) {
        this.fechaFundacion = fechaFundacion;
    }

    public Entrenador getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(Entrenador entrenador) {
        this.entrenador = entrenador;
    }

    @Override
    public String toString() {
        if(entrenador != null){
            return nombre + " - " + entrenador.getNombre();
        }
        return nombre ;
    }
}
