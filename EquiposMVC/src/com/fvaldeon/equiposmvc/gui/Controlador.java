package com.fvaldeon.equiposmvc.gui;

import com.fvaldeon.equiposmvc.base.Entrenador;
import com.fvaldeon.equiposmvc.base.Equipo;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.io.IOException;
import java.util.List;

/**
 * Created by DAM on 25/10/2018.
 */
public class Controlador implements ActionListener, KeyListener, ListSelectionListener, WindowListener{

    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        anadirActionListeners(this);
        anadirKeyListeners(this);
        anadirListSeleccionListeners(this);
        anadirWindowListener(this);

        try {
            modelo.cargarDatos();
            refrescarListaEntrenadores();
            refrescarComboBoxEntrenadores();
            refrescarListaEquipos();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void anadirActionListeners(ActionListener listener){
        vista.btnAltaEntrenador.addActionListener(listener);
        vista.btnGuardarEquipo.addActionListener(listener);
        vista.btnModificarEntrenador.addActionListener(listener);
        vista.btnModificarEquipo.addActionListener(listener);
        vista.btnMostrarEquipos.addActionListener(listener);

    }

    private void anadirKeyListeners(KeyListener listener){
        vista.txtBuscarEntrenador.addKeyListener(listener);
        vista.listEquipos.addKeyListener(listener);
    }

    private void anadirListSeleccionListeners(ListSelectionListener listener){
        vista.listEntrenadores.addListSelectionListener(listener);
        vista.listEquipos.addListSelectionListener(listener);
    }

    private void anadirWindowListener(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando){
            case "GuardarEquipo":
                modelo.altaEquipo(vista.txtNombreEquipo.getText(), vista.txtSedeEquipo.getToolTipText(),vista.dpFechaFundacion.getDate(), (Entrenador) vista.dcbmEntrenador.getSelectedItem());
                borrarDatosEquipo();
                break;
            case "ModificarEquipo": {
                Equipo equipoModificado = (Equipo) vista.listEquipos.getSelectedValue();
                Entrenador entrenadorSeleccionado = (Entrenador) vista.cbEquipoEntrenador.getSelectedItem();
                modelo.modificarEquipo(vista.txtNombreEquipo.getText(), vista.txtSedeEquipo.getText(), vista.dpFechaFundacion.getDate(), entrenadorSeleccionado, equipoModificado);
            }
                break;
            case "AltaEntrenador":
                modelo.altaEntrenador(vista.txtNombreEntrenador.getText(), vista.txtDniEntrenador.getText(), vista.dpFechaEntrenador.getDate());
                borrarDatosEntrenador();
                break;
            case "ModificarEntrenador": {
                Entrenador entrenadorSeleccionado = (Entrenador) vista.listEntrenadores.getSelectedValue();
                modelo.modificarEntrenador(vista.txtNombreEntrenador.getText(), vista.txtDniEntrenador.getText(), vista.dpFechaEntrenador.getDate(), entrenadorSeleccionado);
            }
                break;
            case "MostrarEquipos":{
                if(vista.listEntrenadores.isSelectionEmpty()){
                    JOptionPane.showMessageDialog(null,"Debes seleccionar un entrenador","Error",JOptionPane.ERROR_MESSAGE);
                }else {
                    DialogoEquipos dialogoEquipos = new DialogoEquipos();
                    int resultado = dialogoEquipos.mostrarDialogo(modelo.getListaEquipos());
                    if (resultado == DialogoEquipos.ACEPTAR) {
                        cambiarEntrenadorAEquipos(dialogoEquipos.obtenerEquiposSeleccionados());
                    }
                }

            }
                break;
        }

        refrescarListaEntrenadores();
        refrescarComboBoxEntrenadores();
        refrescarListaEquipos();
    }

    private void cambiarEntrenadorAEquipos(List<Equipo> equipos) {
        for(Equipo equipo : equipos){
            equipo.setEntrenador((Entrenador) vista.listEntrenadores.getSelectedValue());
        }
    }

    /**
     * Control de las teclas. Se usa para el campo de buscar entrenador y para eliminar equipos del JList de equipos
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtBuscarEntrenador){
            if(vista.txtBuscarEntrenador.getText().length() > 2){
                refrescarListaEntrenadores(vista.txtBuscarEntrenador.getText());
            }else{
                refrescarListaEntrenadores();
            }
        }else if(e.getSource() == vista.listEquipos && e.getKeyCode() == KeyEvent.VK_DELETE){
            modelo.eliminarEquipos((Equipo) vista.listEquipos.getSelectedValue());
            refrescarListaEquipos();
            borrarDatosEquipo();
        }
    }

    private void refrescarListaEquipos(){
        vista.dlmEquipo.clear();
        for(Equipo equipo : modelo.getListaEquipos()){
            vista.dlmEquipo.addElement(equipo);
        }
    }

    private void refrescarListaEntrenadores(){
        vista.dlmEntrenador.clear();
        for(Entrenador entrenador : modelo.getListaEntrenadores()){
            vista.dlmEntrenador.addElement(entrenador);
        }
    }

    /**
     * Método que para buscar entrenadores a partir de su nombre
     * @param nombreEntrenador
     */
    private void refrescarListaEntrenadores(String nombreEntrenador){
        vista.dlmEntrenador.clear();
        for(Entrenador entrenador : modelo.getListaEntrenadores(nombreEntrenador)){
            vista.dlmEntrenador.addElement(entrenador);
        }
    }

    private void refrescarComboBoxEntrenadores(){
        vista.dcbmEntrenador.removeAllElements();
        //Anado un etrenador null, por si no se quiere seleccionar entrenador
        vista.dcbmEntrenador.addElement(null);
        for(Entrenador entrenador : modelo.getListaEntrenadores()){
            vista.dcbmEntrenador.addElement(entrenador);
        }
    }

    /**
     * Se muestran los datos del elemento seleccionado de la lista concreta
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listEquipos){
                mostrarDatosEquipoSeleccionado();
            }else{
                vista.txtBuscarEntrenador.setText("");
                mostrarDatosEntrenadorSeleccionado();
            }
        }
    }

    /**
     * Cuando cierro la ventana se guardan los datos en fichero
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        try {
            modelo.guardarDatos();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Obtengo el elemento seleccionado de la lista de Equipos y muestro sus datos
     * en los campos de texto
     */
    private void mostrarDatosEquipoSeleccionado() {
        Equipo equipoSeleccionado = (Equipo) vista.listEquipos.getSelectedValue();
        vista.txtNombreEquipo.setText(equipoSeleccionado.getNombre());
        vista.txtSedeEquipo.setText(equipoSeleccionado.getSede());
        vista.dpFechaFundacion.setDate(equipoSeleccionado.getFechaFundacion());
        vista.cbEquipoEntrenador.setSelectedItem(equipoSeleccionado.getEntrenador());
    }

    /**
     * Obtengo el elemento seleccionado de la lista de entrenadores y muestro sus datos
     * en los campos de texto
     */
    private void mostrarDatosEntrenadorSeleccionado() {
        Entrenador entrenadorSeleccionado = (Entrenador) vista.listEntrenadores.getSelectedValue();
        vista.txtNombreEntrenador.setText(entrenadorSeleccionado.getNombre());
        vista.txtDniEntrenador.setText(entrenadorSeleccionado.getDni());
        vista.dpFechaEntrenador.setDate(entrenadorSeleccionado.getFechaNacimiento());
    }

    private void borrarDatosEntrenador(){
        vista.txtDniEntrenador.setText("");
        vista.txtNombreEntrenador.setText("");
        vista.dpFechaEntrenador.setText("");
    }

    private void borrarDatosEquipo(){
        vista.txtSedeEquipo.setText("");
        vista.txtNombreEquipo.setText("");
        vista.dpFechaFundacion.setText("");
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }
}
