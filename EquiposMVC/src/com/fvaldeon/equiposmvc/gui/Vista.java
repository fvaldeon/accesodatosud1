package com.fvaldeon.equiposmvc.gui;

import com.fvaldeon.equiposmvc.base.Entrenador;
import com.fvaldeon.equiposmvc.base.Equipo;
import com.github.lgooddatepicker.components.DatePicker;
import javax.swing.*;



/**
 * Created by DAM on 19/10/2018.
 */
public class Vista{
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    JFrame frame;
    JTextField txtNombreEquipo;
    JTextField txtSedeEquipo;
    JComboBox cbEquipoEntrenador;
    JList listEquipos;
    JButton btnGuardarEquipo;
    JButton btnModificarEquipo;
    DatePicker dpFechaFundacion;
    JTextField txtNombreEntrenador;
    JTextField txtDniEntrenador;
    JTextField txtBuscarEntrenador;
    JList listEntrenadores;
    JButton btnModificarEntrenador;
    JButton btnAltaEntrenador;
    DatePicker dpFechaEntrenador;
    JButton btnMostrarEquipos;
    DefaultListModel<Equipo> dlmEquipo;
    DefaultListModel<Entrenador> dlmEntrenador;
    DefaultComboBoxModel<Entrenador> dcbmEntrenador;



    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        iniciarModelos();
        //Mostrar Jframe en el centro de la pantalla
        frame.setLocationRelativeTo(null);


    }

    private void iniciarModelos() {
        dlmEntrenador = new DefaultListModel<>();
        dlmEquipo = new DefaultListModel<>();
        dcbmEntrenador = new DefaultComboBoxModel<>();

        listEntrenadores.setModel(dlmEntrenador);
        listEquipos.setModel(dlmEquipo);
        cbEquipoEntrenador.setModel(dcbmEntrenador);
    }


}
