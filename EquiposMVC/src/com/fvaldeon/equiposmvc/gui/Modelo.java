package com.fvaldeon.equiposmvc.gui;

import com.fvaldeon.equiposmvc.base.Entrenador;
import com.fvaldeon.equiposmvc.base.Equipo;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by DAM on 25/10/2018.
 */
public class Modelo {
    private ArrayList<Equipo> listaEquipos;
    private ArrayList<Entrenador> listaEntrenadores;


    public void altaEntrenador(String nombre, String dni, LocalDate fecha){
        listaEntrenadores.add(new Entrenador(nombre, dni, fecha));
    }

    public void altaEquipo(String nombre, String sede, LocalDate fecha, Entrenador entrenador){
        listaEquipos.add(new Equipo(nombre, sede, fecha, entrenador));

    }

    public void altaEquipo(String nombre, String sede, LocalDate fecha){
        listaEquipos.add(new Equipo(nombre, sede, fecha));

    }

    public void guardarDatos() throws IOException {
        FileOutputStream fos = new FileOutputStream("datos.bin");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(listaEntrenadores);
        oos.writeObject(listaEquipos);
        fos.close();
    }

    public void cargarDatos() throws IOException, ClassNotFoundException {

        File fichero = new File("datos.bin");

        if(fichero.exists()) {
            FileInputStream fis = new FileInputStream(fichero);
            ObjectInputStream ois = new ObjectInputStream(fis);
            listaEntrenadores = (ArrayList<Entrenador>) ois.readObject();
            listaEquipos = (ArrayList<Equipo>) ois.readObject();
            fis.close();
        }else{
            listaEquipos = new ArrayList<>();
            listaEntrenadores = new ArrayList<>();
        }
    }

    public void modificarEntrenador(String nombre, String dni, LocalDate fecha, Entrenador entrenador){
        entrenador.setDni(dni);
        entrenador.setFechaNacimiento(fecha);
        entrenador.setNombre(nombre);
    }

    public void modificarEquipo(String nombre, String sede, LocalDate fecha, Entrenador entrenador, Equipo equipo){
        equipo.setEntrenador(entrenador);
        equipo.setFechaFundacion(fecha);
        equipo.setNombre(nombre);
        equipo.setSede(sede);
    }

    public void eliminarEntrenador(Entrenador entrenador){
        listaEntrenadores.remove(entrenador);
    }

    public void eliminarEquipos(Equipo equipo){
        listaEquipos.remove(equipo);
    }

    public ArrayList<Entrenador> getListaEntrenadores() {
        return listaEntrenadores;
    }

    /**
     * Método que recibe el nombre de un entrenador y devuelve una lista de entrenadores
     * cuyo nombre comienza por esa cadena
     * @param nombre El nombre a buscar
     * @return la lista con los entrenadores
     */
    public ArrayList<Entrenador> getListaEntrenadores(String nombre) {
        ArrayList<Entrenador> entrenadoresBuscados = new ArrayList<>();
        for( Entrenador entrenador : listaEntrenadores){
            if(entrenador.getNombre().startsWith(nombre)){
                entrenadoresBuscados.add(entrenador);
            }
        }
        return entrenadoresBuscados;
    }

    public ArrayList<Equipo> getListaEquipos() {
        return listaEquipos;
    }


}
