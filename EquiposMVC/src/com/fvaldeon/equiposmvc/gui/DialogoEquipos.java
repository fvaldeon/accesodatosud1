package com.fvaldeon.equiposmvc.gui;

import com.fvaldeon.equiposmvc.base.Equipo;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class DialogoEquipos extends JDialog {
    public final static int ACEPTAR = 1;
    public final static int CANCELAR = 0;

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JList list1;

    private DefaultListModel<Equipo> dlmEquipos;
    private int estado;
    private List<Equipo> equipos;
    private List<Equipo> equiposSeleccionados;

    public DialogoEquipos() {

        dlmEquipos = new DefaultListModel<>();
        list1.setModel(dlmEquipos);

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        setLocationRelativeTo(null);

        pack();
    }

    private void onOK() {
        estado = ACEPTAR;
        equiposSeleccionados = list1.getSelectedValuesList();

        dispose();
    }

    private void onCancel() {
        estado = CANCELAR;
        dispose();
    }


    public int mostrarDialogo(List<Equipo> equipos){
        this.equipos = equipos;
        mostrarEquipos();

        setVisible(true);
        return estado;
    }

    private void mostrarEquipos() {
        dlmEquipos.clear();
        for (Equipo equipo : equipos) {
            dlmEquipos.addElement(equipo);
        }
    }

    public List<Equipo> obtenerEquiposSeleccionados(){
        return equiposSeleccionados;
    }

}
