package com.fvaldeon.loginconf;

import javax.swing.*;
import java.awt.event.*;

public class DialogoDatos extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPasswordField passwordField;
    private JTextField usuariotxt;
    private JTextField direccionTxt;

    public enum tipoEstado {ACEPTAR, CANCELAR};
    //Privados
    private String usuario;
    private String direccion;
    private String password;
    private tipoEstado estado;

    public tipoEstado getEstado(){
        return estado;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getPassword() {
        return password;
    }

    public DialogoDatos() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    public void mostrarDialogo(){
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void onOK() {
        this.usuario = usuariotxt.getText();
        this.password = String.valueOf(passwordField.getPassword());
        this.direccion = direccionTxt.getText();
        passwordField.setText("");

        estado = tipoEstado.ACEPTAR;
        dispose();
    }

    private void onCancel() {
        estado = tipoEstado.CANCELAR;
        passwordField.setText("");
        dispose();
    }
}
