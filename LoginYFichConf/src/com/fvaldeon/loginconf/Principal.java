package com.fvaldeon.loginconf;

import java.io.IOException;

/**
 * Created by Profesor on 24/10/2017.
 */
public class Principal {
    public static void main(String[] args) {
        VentanaPrincipal vista = new VentanaPrincipal();
        try {
            Controlador controlador = new Controlador(vista);
        } catch (IOException e) {
            System.out.println("No existe fichero de configuracion: " + e.getMessage());

        }
    }

}
