package com.fvaldeon.loginconf;

import javax.swing.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

/**
 * Created by Profesor on 24/10/2017.
 */
public class Controlador extends WindowAdapter implements ActionListener{

    VentanaPrincipal vista;

    public Controlador(VentanaPrincipal vista) throws IOException {
        this.vista = vista;

        vista.abrirDialogoBtn.addActionListener(this);
        vista.frame.addWindowListener(this);

        cargarConfiguracion();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        DialogoDatos dialogo = new DialogoDatos();

        dialogo.mostrarDialogo();

        if(dialogo.getEstado() == DialogoDatos.tipoEstado.ACEPTAR){
           vista.direccionTxt.setText(dialogo.getDireccion());
           vista.usuarioTxt.setText(dialogo.getUsuario());
           vista.passwordField.setText(dialogo.getPassword());

        }


    }

    private void guardarConfiguracion() throws IOException {
        Properties propiedades = new Properties();
        propiedades.setProperty("usuario",vista.usuarioTxt.getText());
        propiedades.setProperty("password", String.valueOf(vista.passwordField.getPassword()));
        propiedades.setProperty("direccion",vista.direccionTxt.getText());

        propiedades.store(new PrintWriter("login.conf"), "Datos de conexion");
    }

    private void cargarConfiguracion() throws IOException {
        Properties propiedades = new Properties();

        propiedades.load(new FileReader("login.conf"));

        vista.usuarioTxt.setText(propiedades.getProperty("usuario"));
        vista.passwordField.setText(propiedades.getProperty("password"));
        vista.direccionTxt.setText(propiedades.getProperty("direccion"));

    }

    @Override
    public void windowClosing(WindowEvent e) {
        super.windowClosing(e);
        try {
            guardarConfiguracion();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
