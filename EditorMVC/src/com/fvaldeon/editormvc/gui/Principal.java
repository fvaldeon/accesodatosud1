package com.fvaldeon.editormvc.gui;

/**
 * Created by DAM on 05/10/2018.
 */
public class Principal {

    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }

}
