package com.fvaldeon.editormvc.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by DAM on 05/10/2018.
 */
public class Controlador implements ActionListener{

    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;

        asociarListeners(this);
    }

    private void asociarListeners(ActionListener listener){
        vista.itemAbrir.addActionListener(listener);
        vista.itemGuardar.addActionListener(listener);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String accion = e.getActionCommand();

        switch (accion){
            case "abrir":
            {
                JFileChooser selectorFichero = new JFileChooser();
                int opcion = selectorFichero.showOpenDialog(null);

                if(opcion == JFileChooser.APPROVE_OPTION){

                    File fichero = selectorFichero.getSelectedFile();

                    try {
                        vista.txtArea.setText(modelo.cargarTexto(fichero));
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }

            }
                break;
            case "guardar":
            {
                JFileChooser selectorFichero = new JFileChooser();
                int opt = selectorFichero.showSaveDialog(null);

                if( opt == JFileChooser.APPROVE_OPTION){
                    File ficheroSeleccionado = selectorFichero.getSelectedFile();
                    try {

                        modelo.guardarTexto(ficheroSeleccionado, vista.txtArea.getText());

                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }

            }
                break;
        }
    }
}
