package com.fvaldeon.editormvc.gui;




import javax.swing.*;

/**
 * Created by DAM on 05/10/2018.
 */
public class Vista {
    private JPanel panel1;
    JTextArea txtArea;
    private JFrame frame;
    JMenuItem itemGuardar;
    JMenuItem itemAbrir;

    /**
     * Constructor de la clase Vista
     */
    public Vista() {
        frame = new JFrame("EditorMVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();

        crearBarraMenu();
        frame.setVisible(true);
        //Me emplaza la ventana en el centro de la pantalla
        frame.setLocationRelativeTo(null);
    }

    /**
     * Metodo para crear la barra de menu
     */
    private void crearBarraMenu() {
        JMenuBar barra = new JMenuBar();

        JMenu menu = new JMenu("Archivo");

        itemGuardar = new JMenuItem("Guardar");
        itemAbrir = new JMenuItem("Abrir");

        //Anado los action commands para poder
        // reconocer que boton ha producido el evento
        itemGuardar.setActionCommand("guardar");
        itemAbrir.setActionCommand("abrir");

        menu.add(itemAbrir);
        menu.add(itemGuardar);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }
}
