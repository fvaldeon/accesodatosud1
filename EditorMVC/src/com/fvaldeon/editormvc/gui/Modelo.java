package com.fvaldeon.editormvc.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by DAM on 05/10/2018.
 */
public class Modelo {

    /**
     * Método que escribe texto en un fichero
     * @param fichero objeto file sobre el que escribo el texto
     * @param texto String con el texto a escribir
     * @throws FileNotFoundException si el fichero no existe
     */
    public void guardarTexto(File fichero, String texto) throws FileNotFoundException {
        PrintWriter escritor = new PrintWriter(fichero);

        escritor.println(texto);

        escritor.close();
    }

    /**
     * Método que lee texto de un fichero y lo devuelve en String
     * @param fichero objeto file sobre el que escribo
     * @return todo el texto en formato String leido de ese fichero
     * @throws FileNotFoundException si el fichero no existe
     */
    public String cargarTexto(File fichero) throws FileNotFoundException {
        Scanner lector = new Scanner(fichero);
        StringBuilder textoLeido = new StringBuilder();

        while(lector.hasNextLine()){
            textoLeido.append(lector.nextLine());
        }

        lector.close();
        return textoLeido.toString();
    }

}
